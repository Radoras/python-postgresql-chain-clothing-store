"""
File with converting functions
"""

import re


def enum_class_to_tuple_of_strings(enum_class):
    return tuple(element.name.lower() for element in enum_class)


def python_type_to_sql_type(python_type_name):
    return "_".join(l.lower() for l in re.findall('[A-Z][^A-Z]*', python_type_name))
