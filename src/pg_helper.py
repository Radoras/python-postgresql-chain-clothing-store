"""
File with actions that support the psycopg2 library in this project
"""


def fetchall_single_column(cursor):
    return [x[0] for x in cursor.fetchall()]


def generic_sql(conn, query, args=None):
    cursor = conn.cursor()

    cursor.execute(query, args)

    conn.commit()
    cursor.close()
