"""
Data and data manipulation functions for data randomization in psql_tables
"""

import datetime
import random
import string
import math

female_names = ['Anna', 'Molly', 'Elizabeth', 'Rebecca', 'Amber', 'Ella', 'Nicole', 'Martha', 'Hannah', 'Jasmine',
                'Amy', 'Sophie', 'Julia', 'Luna', 'Eliza', 'Emma', 'Sara', 'Gwen', 'Jessie', 'Vera', 'Karen',
                'Alice', 'Claudia', 'Lily', 'Alexa', 'Helena', 'Collins', 'Bethany', 'Natalie']

male_names = ['Jack', 'Olivier', 'Jacob', 'John', 'William', 'Richard', 'Edward', 'Luke', 'Thomas', 'Adam', 'David',
              'Daniel', 'Justin', 'Nolan', 'Pablo', 'Lukas', 'Hugh', 'Arthur', 'Roger', 'Ronald', 'Steve', 'Dustin',
              'Elliot', 'Nico', 'Clark', 'Eric', 'Andrew', 'Zak', 'Maximilian', 'Christopher', 'Ian', 'Bowen']

surnames = ['Evans', 'Parker', 'Roberts', 'Jackson', 'Jenkins', 'Smith', 'Wright', 'Carter', 'Bailet', 'Perry',
            'James', 'Rogers', 'Jones', 'Young', 'Barnes', 'Wood', 'Anderson', 'Williams', 'Brown', 'Reed',
            'Lee', 'Turner', 'King', 'Collins', 'Wilson', 'Lopez', 'Ford', 'Spencer', 'Webb', 'Moore']

existing_emails = set()

existing_barcodes = set()


countries = ["United States"]

cities = ["New York", "Los Angeles", "Chicago", "Houston", "Phoenix", "Philadelphia", "San Antonio", "San Diego",
          "Dallas", "San Jose", "Austin", "Jacksonville", "Fort Worth", "Columbus", "Charlotte", "San Francisco",
          "Indianapolis", "Seattle", "Denver", "Washington", "Boston", "El Paso", "Nashville", "Detroit"]

street_names = ["Chapet Street", "Oakwood Road", "St Andrew's Close", "Woodside", "St Paul's Road", "Home Close",
                "Mansfield Road", "Coronation Street", "Cambridge Street", "Grasmere Road", "Poplar Drive",
                "Waverley Road", "The Ridgeway", "The Croft", "Victoria Place", "Queensway", "Walnut Close",
                "Conway Close", "Oak Avenue", "Albion Street", "Derwent Close", "Wentworth Road", "Chestnut Grove"]


clothing_brands = ["Nike", "Adidas", "Puma", "ZARA", "Armani", "GUESS", "House", "Cropp", "Calvin Klein",
                   "The North Face", "Prada", "Under Armour", "Levi's", "H&M", "Uniqlo", "Bershka"]
accessories_brands = ["Hermes", "Louis Vuitton", "GUCCI", "Prada", "Michael Kors", "Coach", "Louis Vuitton"]
jewellery_brands = ["Cartier", "Swarovski", "Chow Tai Fook", "Tiffany & Co.", "David Yurman", "Mikimoto", "Hermes"]


def random_date(start, end):
    delta = abs(end - start)
    random_days = random.randrange(delta.days)
    return start + datetime.timedelta(days=random_days)


def generate_random_address():
    return {'country': random.choice(countries),
            'city': random.choice(cities),
            'postal_code': ''.join(random.choice(string.digits) for _ in range(5)),
            'street_name': random.choice(street_names),
            'street_number': random.randint(1, 300),
            'apartment_number': random.randint(1, 100)}


def generate_email_address(name, surname, year_of_birth):
    domains = ['gmail.com', 'yahoo.com', 'hotmail.com', 'aol.com']
    email = None
    i = 0

    if name:
        while not email or email in existing_emails:
            domain = random.choice(domains)

            number_of_letters_from_name = random.choice([1, 2, 3, len(name)])
            email_name = name[:number_of_letters_from_name]

            with_separator = random.choices([True, False], [70, 30])[0]
            separator = random.choice('_.-') if with_separator else ''

            number_of_letters_from_surname = random.choice([1, 2, 3, len(name)])
            email_surname = surname[:number_of_letters_from_surname]

            email = email_name + separator + email_surname

            i += 1
            if i >= 100:
                email += ''.join(random.choices(string.digits, k=random.randint(1, 5)))
            else:
                len_of_year = len(str(year_of_birth))
                number_of_letters_from_year = random.choice([0, 2, len_of_year])
                email_year = str(year_of_birth)[:number_of_letters_from_year] if number_of_letters_from_year > 0 else ''
                email += email_year

            email += '@' + domain
    else:
        while not email or email in existing_emails:
            domain = random.choice(domains)

            characters = string.ascii_letters + string.digits
            email = ''.join(random.choices(characters, k=random.randint(10, 15))) + '@' + domain

    existing_emails.add(email)
    return email


def generate_phone_number():
    return ''.join(random.choices(string.digits, k=random.randint(8, 10)))


def generate_account_number():
    return ''.join(random.choices(string.digits, k=random.randint(10, 12)))


def generate_barcode():
    barcode = None
    while not barcode or barcode in existing_barcodes:
        barcode = ''.join(random.choices(string.digits, k=10))

    existing_barcodes.add(barcode)
    return barcode
