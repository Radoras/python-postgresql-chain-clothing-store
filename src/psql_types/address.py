"""
Address type
"""

from pg_helper import *

type_name = 'address'


def create_type(conn):
    generic_sql(conn, """
        CREATE TYPE {} AS (
            country VARCHAR(14),
            city VARCHAR(30),
            postal_code CHAR(5),
            street_name VARCHAR(40),
            street_number SMALLINT,
            apartment_number SMALLINT
        )""".format(type_name))
