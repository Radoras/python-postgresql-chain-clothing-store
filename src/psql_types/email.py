"""
Email domain
"""

from pg_helper import *

domain_name = 'email'


def create_domain(conn):
    generic_sql(conn, ("""
        CREATE DOMAIN {} AS VARCHAR(100)
        CHECK (VALUE ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{{|}}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{{0,61}}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{{0,61}}[a-zA-Z0-9])?)*$')""").format(domain_name)
    )
