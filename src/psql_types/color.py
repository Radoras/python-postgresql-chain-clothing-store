"""
Color type
"""

from enum import Enum

import converter
from pg_helper import *

type_name = 'color'


class Color(Enum):
    BLACK = 1,
    BLUE = 2,
    BROWN = 3,
    GRAY = 4,
    GREEN = 5,
    ORANGE = 6,
    PINK = 7,
    PURPLE = 8,
    RED = 9,
    WHITE = 10,
    YELLOW = 11


def create_type(conn):
    type_elements = converter.enum_class_to_tuple_of_strings(Color)
    generic_sql(conn, "CREATE TYPE {} AS ENUM {}".format(type_name, type_elements))
