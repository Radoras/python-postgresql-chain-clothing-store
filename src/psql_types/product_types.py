"""
Product type tables
"""

from enum import Enum

import converter

product_types = {'TrousersType', 'ShirtType', 'HatType', 'UnderwearType', 'JewelleryType', 'JewelleryMaterial',
                 'AccessoriesType', 'AccessoriesMaterial'}


class TrousersType(Enum):
    FIVE_POCKET_JEANS = 1,
    HOT_PANTS = 2,
    SAILOR_PANTS = 3,
    SHORTS = 4,
    SKINNY = 5,
    STRAIGHT = 6


class ShirtType(Enum):
    JACKET = 1,
    LONG_SLEEVE = 2,
    POLO = 3,
    SHIRT = 4,
    SLEEVELESS = 5,
    SWEATSHIRT = 6,
    T_SHIRT = 7,
    TANK_TOP = 8


class HatType(Enum):
    BASEBALL = 1,
    BEANIE = 2,
    BERET = 3,
    BOATER = 4,
    COWBOY = 5,
    FLOPPY = 6,
    PANAMA = 7,
    TOP = 8,
    TRAPPER = 9


class UnderwearType(Enum):
    BOXERS = 1,
    BRA = 2,
    PANTIES = 3,
    SOCKS = 4


class ClotheMaterial(Enum):
    CHIFFON = 0,
    COTTON = 1,
    CREPE = 2,
    DENIM = 3,
    GEORGETTE = 4,
    LEATHER = 5,
    LINEN = 6,
    LYCRA = 7,
    NET = 8,
    NYLON = 9,
    POLYESTER = 10,
    RAYON = 11,
    SATIN = 12,
    SILK = 13,
    VELVET = 14,
    VISCOSE = 15,
    WOOL = 16


class JewelleryType(Enum):
    BRACELET = 1,
    DIADEM = 2,
    EARRINGS = 3,
    HAIR_CLIP = 4,
    NECKLACE = 5,
    RING = 6


class JewelleryMaterial(Enum):
    AMBER = 1,
    AMETHYST = 2,
    COPPER = 3,
    DIAMOND = 4,
    EMERALD = 5,
    GOLD = 6,
    PEARL = 7,
    PLATINUM = 8,
    RUBY = 9,
    SAPPHIRE = 10,
    SHELL = 11,
    SILVER = 12,
    TITANIUM = 13


class AccessoriesType(Enum):
    BAG = 1,
    GLOVES = 2,
    SCARF = 3


class AccessoriesMaterial(Enum):
    COTTON = 1,
    JEANS = 2,
    LEATHER = 3


def create_types(conn):
    cursor = conn.cursor()

    for product_type in product_types:
        psql_types = converter.enum_class_to_tuple_of_strings(eval(product_type))
        psql_type_name = converter.python_type_to_sql_type(product_type)
        cursor.execute("CREATE TYPE {} AS ENUM {}".format(psql_type_name, tuple(psql_types)))

    conn.commit()
    cursor.close()


def drop_types(conn):
    cursor = conn.cursor()

    for product_type in product_types:
        psql_type_name = converter.python_type_to_sql_type(product_type)
        cursor.execute("DROP TYPE IF EXISTS {}".format(psql_type_name))

    conn.commit()
    cursor.close()
