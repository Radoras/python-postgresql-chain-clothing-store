"""
File related to creating and deleting types
"""

import converter
import psql_types

types = {'address', 'color', 'email'}
types.update(converter.python_type_to_sql_type(product_type) for product_type in psql_types.product_types.product_types)


def does_all_types_exist(conn):
    cursor = conn.cursor()

    cursor.execute("SELECT typname FROM pg_type WHERE typname IN %s", [tuple(types)])
    tables_info = cursor.fetchall()
    tables_names = set(row[0] for row in tables_info)

    conn.commit()
    cursor.close()

    return tables_names == types


def create_types(conn):
    psql_types.address.create_type(conn)
    psql_types.color.create_type(conn)
    psql_types.product_types.create_types(conn)
    psql_types.email.create_domain(conn)


def drop_types(conn):
    cursor = conn.cursor()

    for psql_type in types:
        cursor.execute("DROP TYPE IF EXISTS {} CASCADE".format(psql_type))

    conn.commit()
    cursor.close()


def recreate_types(conn):
    drop_types(conn)
    create_types(conn)
