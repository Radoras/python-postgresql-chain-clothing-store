"""
Products table
"""

from abc import ABC, abstractmethod

from data import *
from pg_helper import *
from psql_tables.table import Table


class Product(Table, ABC):
    _table_name = 'products'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    @abstractmethod
    def insert(self, cursor):
        pass

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE products (
                barcode CHAR(10) PRIMARY KEY CHECK(barcode ~ '^[0-9]*$'),
                selling_price FLOAT NOT NULL CHECK(selling_price > 0),
                order_price FLOAT NOT NULL CHECK(order_price > 0),
                gender CHAR(1) NOT NULL CHECK(gender IN ('m', 'f', 'u')),
                brand VARCHAR(20),
                CHECK (selling_price > order_price)
            )""")

    @staticmethod
    @abstractmethod
    def _randomize_data_row(conn):
        data = dict()
        data['barcode'] = generate_barcode()
        data['gender'] = random.choice(['m', 'f', 'u'])

        return data
