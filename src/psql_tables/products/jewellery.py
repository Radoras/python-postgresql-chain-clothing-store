"""
Jewellery table
"""

from data import *
from pg_helper import *
from psql_tables.products.products import Product
from psql_types.product_types import JewelleryType, JewelleryMaterial


class Jewellery(Product):
    _table_name = 'jewellery'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        cols = ['barcode', 'selling_price', 'order_price', 'gender', 'brand', 'type', 'material']

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        cursor.execute("INSERT INTO jewellery VALUES (%s, %s, %s, %s, %s, %s, %s::VARCHAR(15)[])",
                       [full_data[col] for col in cols])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE jewellery (
                type jewellery_type,
                material VARCHAR(15) ARRAY
            ) INHERITS (products)""")

    @staticmethod
    def _randomize_data_row(conn):
        data = Product._randomize_data_row(conn)

        data['brand'] = random.choice(jewellery_brands)

        data['type'] = random.choice(list(JewelleryType))
        data['material'] = random.choices(list(JewelleryMaterial), k=random.randint(1, 4))

        order_price = 0
        if data['type'] == JewelleryType.BRACELET:
            order_price += random.randint(3, 6)
        elif data['type'] == JewelleryType.DIADEM:
            order_price += random.randint(5, 8)
        elif data['type'] == JewelleryType.EARRINGS:
            order_price += random.randint(1, 4)
        elif data['type'] == JewelleryType.HAIR_CLIP:
            order_price += random.randint(2, 5)
        elif data['type'] == JewelleryType.NECKLACE:
            order_price += random.randint(5, 8)
        elif data['type'] == JewelleryType.RING:
            order_price += random.randint(2, 5)

        price_multiplier = 1
        if JewelleryMaterial.AMBER in data['material']:
            price_multiplier += random.randint(5, 8)
        elif JewelleryMaterial.AMETHYST in data['material']:
            price_multiplier += random.randint(5, 10)
        elif JewelleryMaterial.COPPER in data['material']:
            price_multiplier += random.randint(4, 6)
        elif JewelleryMaterial.DIAMOND in data['material']:
            price_multiplier += random.randint(70, 180)
        elif JewelleryMaterial.EMERALD in data['material']:
            price_multiplier += random.randint(11, 24)
        elif JewelleryMaterial.GOLD in data['material']:
            price_multiplier += random.randint(9, 22)
        elif JewelleryMaterial.PEARL in data['material']:
            price_multiplier += random.randint(9, 16)
        elif JewelleryMaterial.PLATINUM in data['material']:
            price_multiplier += random.randint(22, 55)
        elif JewelleryMaterial.RUBY in data['material']:
            price_multiplier += random.randint(16, 61)
        elif JewelleryMaterial.SAPPHIRE in data['material']:
            price_multiplier += random.randint(7, 18)
        elif JewelleryMaterial.SHELL in data['material']:
            price_multiplier += random.randint(1, 4)
        elif JewelleryMaterial.SILVER in data['material']:
            price_multiplier += random.randint(4, 12)
        elif JewelleryMaterial.TITANIUM in data['material']:
            price_multiplier += random.randint(3, 7)

        price_multiplier /= len(data['material'])
        order_price *= price_multiplier
        order_price = int(order_price)

        selling_price = order_price * (random.randint(110, 165) / 100)
        data['order_price'] = order_price
        data['selling_price'] = math.ceil(selling_price)

        data['type'] = data['type'].name.lower()

        for i, material in enumerate(data['material']):
            lower_name = material.name.lower()
            data['material'][i] = lower_name

        return data
