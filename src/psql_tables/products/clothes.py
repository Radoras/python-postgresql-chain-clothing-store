"""
Clothes table
"""

import json
from abc import ABC, abstractmethod

from data import *
from pg_helper import *
from psql_tables.products.products import Product
from psql_types.color import Color
from psql_types.product_types import *


class Clothe(Product, ABC):
    _table_name = 'clothes'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        cols = ['barcode', 'selling_price', 'order_price', 'gender', 'brand', 'sizes', 'colors', 'fabric', 'type']

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        full_data['sizes'] = list(full_data['sizes'])
        full_data['colors'] = list(full_data['colors'])

        fabric_json = json.dumps(full_data['fabric'])
        full_data['fabric'] = fabric_json

        cursor.execute("INSERT INTO {} VALUES (%s, %s, %s, %s, %s, %s::VARCHAR(3)[], %s::Color[], %s, %s)"
                       .format(self._table_name), [full_data[col] for col in cols])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE clothes (
                sizes VARCHAR(3) ARRAY NOT NULL CHECK(sizes <@ ARRAY['xs', 's', 'm', 'l', 'xl', 'xxl']::VARCHAR(3)[]),
                colors color ARRAY,
                fabric JSONB    -- key: fabric type; value: stretch percentage
            ) INHERITS (products)""")

    @staticmethod
    @abstractmethod
    def _randomize_data_row(conn):
        data = Product._randomize_data_row(conn)

        data['brand'] = random.choice(clothing_brands)

        available_sizes = ['xs', 's', 'm', 'l', 'xl', 'xxl']
        sizes = random.sample(available_sizes, k=random.randint(1, len(available_sizes)))
        data['sizes'] = [size for size in available_sizes if size in sizes]    # keep order

        available_colors = [col.name.lower() for col in Color]
        data['colors'] = random.sample(available_colors, k=random.randint(1, 4))

        data['fabric'] = Clothe.__generate_fabric()

        return data

    @staticmethod
    def __generate_fabric():
        available_fabrics = [f.name for f in ClotheMaterial]
        max_fabrics = 4
        min_percentage = 5

        fabric = random.sample(available_fabrics, k=random.randint(1, max_fabrics))
        fabric_composition = dict.fromkeys(fabric, min_percentage)

        percentages_left = 100 - len(fabric) * min_percentage
        for fck in fabric_composition.keys():
            if percentages_left:
                extra_percentage = random.randint(0, percentages_left)
                percentages_left -= extra_percentage
                fabric_composition[fck] += extra_percentage
        if percentages_left:
            fabric_composition[min(fabric_composition, key=fabric_composition.get)] += percentages_left

        return fabric_composition
