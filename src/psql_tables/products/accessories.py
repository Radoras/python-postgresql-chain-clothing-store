"""
Accessories table
"""

from data import *
from pg_helper import *
from psql_tables.products.products import Product
from psql_types.product_types import AccessoriesType, AccessoriesMaterial


class Accessory(Product):
    _table_name = 'accessories'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        cols = ['barcode', 'selling_price', 'order_price', 'gender', 'brand', 'type', 'material']

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        cursor.execute("""INSERT INTO accessories (barcode, selling_price, order_price, gender, brand, type, material)
                          VALUES (%s, %s, %s, %s, %s, %s, %s)""",
                       [full_data[col] for col in cols])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE accessories (
                type accessories_type,
                material VARCHAR(15)
            ) INHERITS (products)""")

    @staticmethod
    def _randomize_data_row(conn):
        data = Product._randomize_data_row(conn)

        data['brand'] = random.choice(accessories_brands)

        data['type'] = random.choice(list(AccessoriesType))
        if data['type'] == AccessoriesType.SCARF:
            data['material'] = AccessoriesMaterial.COTTON
        else:
            data['material'] = random.choice(list(AccessoriesMaterial))

        order_price = 0
        if data['type'] == AccessoriesType.BAG:
            order_price += random.randint(4, 9)
        elif data['type'] == AccessoriesType.GLOVES:
            order_price += random.randint(1, 2)
        elif data['type'] == AccessoriesType.SCARF:
            order_price += random.randint(4, 20)

        if data['material'] == AccessoriesMaterial.COTTON:
            order_price *= random.randint(1, 2)
        elif data['material'] == AccessoriesMaterial.JEANS:
            order_price *= random.randint(3, 6)
        elif data['material'] == AccessoriesMaterial.LEATHER:
            order_price *= random.randint(20, 80)

        selling_price = order_price * (random.randint(110, 165) / 100)
        data['order_price'] = order_price
        data['selling_price'] = math.ceil(selling_price)

        data['type'] = data['type'].name.lower()
        data['material'] = data['material'].name.lower()

        return data
