"""
Hats table
"""

import math
import random

from pg_helper import *
from psql_tables.products.clothes import Clothe
from psql_types.product_types import *


class Hat(Clothe):
    _table_name = 'hats'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE hats (
                type hat_type
            ) INHERITS (clothes)""")

    @staticmethod
    def _randomize_data_row(conn):
        data = Clothe._randomize_data_row(conn)

        data['type'] = random.choice(list(HatType))

        data['order_price'] = random.randint(4, 20)
        selling_price = data['order_price'] * (random.randint(110, 165) / 100)
        data['selling_price'] = math.ceil(selling_price)

        data['type'] = data['type'].name.lower()

        return data
