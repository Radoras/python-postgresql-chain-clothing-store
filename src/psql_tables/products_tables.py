"""
Products tables
"""

from psql_tables.products import *
from psql_tables.products import clothes, products

tables = {'products', 'clothes', 'trousers', 'shirts', 'hats', 'underwear', 'jewellery', 'accessories'}


def create_tables(conn):
    products.Product.create_table(conn)
    clothes.Clothe.create_table(conn)
    hats.Hat.create_table(conn)
    shirts.Shirt.create_table(conn)
    trousers.Trousers.create_table(conn)
    underwear.Underwear.create_table(conn)
    accessories.Accessory.create_table(conn)
    jewellery.Jewellery.create_table(conn)


def fill_tables_with_random_data(conn, min_products, max_products):
    # x * (50/100) means that about 1/2 of all types of products are of this type
    hats.Hat.fill_with_random_data(conn, int(min_products * (3 / 100)), int(max_products * (3 / 100)))
    shirts.Shirt.fill_with_random_data(conn, int(min_products * (50 / 100)), int(max_products * (50 / 100)))
    trousers.Trousers.fill_with_random_data(conn, int(min_products * (20 / 100)), int(max_products * (20 / 100)))
    underwear.Underwear.fill_with_random_data(conn, int(min_products * (10 / 100)), int(max_products * (10 / 100)))
    accessories.Accessory.fill_with_random_data(conn, int(min_products * (10 / 100)), int(max_products * (10 / 100)))
    jewellery.Jewellery.fill_with_random_data(conn, int(min_products * (7 / 100)), int(max_products * (7 / 100)))
