"""
Customers table
"""

import logging

from dateutil.relativedelta import relativedelta

from data import *
from pg_helper import *
from psql_tables.table import Table


class Customer(Table):
    _table_name = 'customers'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        customer_data_cols = ['email', 'birth_date', 'name', 'surname']
        address_cols = ['country', 'city', 'postal_code', 'street_name', 'street_number', 'apartment_number']
        cols = customer_data_cols + address_cols

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        if full_data['country']:
            cursor.execute("""INSERT INTO {} (email, birth_date, name, surname, address)
                              VALUES (%s, %s, %s, %s, ROW(%s, %s, %s, %s, %s, %s))""".format(Customer._table_name),
                           [full_data[col] for col in cols])
        else:
            cursor.execute("""INSERT INTO {} (email, birth_date, name, surname)
                              VALUES (%s, %s, %s, %s)""".format(Customer._table_name),
                           [full_data[col] for col in customer_data_cols])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
                CREATE TABLE {} (
                    email email PRIMARY KEY,
                    total_payment FLOAT NOT NULL DEFAULT 0 CHECK (total_payment >= 0),
                    birth_date DATE NOT NULL CHECK (birth_date <= CURRENT_DATE - INTERVAL '12 years'),
                    name VARCHAR(40) CHECK (name ~ '^[A-Za-z]+$'),
                    surname VARCHAR(40) CHECK (surname ~ '^[A-Za-z]+$'),
                    address ADDRESS
                )""".format(Customer._table_name))

    @staticmethod
    def create_triggers(conn):
        cursor = conn.cursor()

        cursor.execute("""
            CREATE OR REPLACE FUNCTION set_total_payment_to_zero()
                RETURNS TRIGGER
                LANGUAGE plpgsql AS
            $BODY$
            BEGIN
                NEW.total_payment := 0.0;
                RETURN NEW;
            END;
            $BODY$""")

        cursor.execute("DROP TRIGGER IF EXISTS block_insert_total_payment ON {}".format(Customer._table_name))
        cursor.execute("""
            CREATE TRIGGER block_insert_total_payment
                BEFORE INSERT ON {}
                FOR EACH ROW
                EXECUTE PROCEDURE set_total_payment_to_zero()
        """.format(Customer._table_name))

        conn.commit()
        cursor.close()

    @staticmethod
    def _randomize_data_row(conn):
        highest_birth_date = datetime.date.today() - relativedelta(years=12)
        lowest_birth_date = datetime.date.today() - relativedelta(years=80)
        birth_date = random_date(lowest_birth_date, highest_birth_date)

        gender = random.choice(['m', 'f'])
        gave_name = random.choices([True, False], [70, 30])[0]
        if gave_name:
            name = random.choice(male_names if gender == 'm' else female_names)
            surname = random.choice(surnames)
        else:
            name = None
            surname = None

        gave_address = random.choices([True, False], [40, 60])[0]
        address = generate_random_address() if gave_address else None

        email = generate_email_address(name, surname, birth_date.year)

        if address:
            return {**{'email': email, 'birth_date': birth_date, 'name': name, 'surname': surname}, **address}
        else:
            return {'email': email, 'birth_date': birth_date, 'name': name, 'surname': surname}
