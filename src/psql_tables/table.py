"""
Abstract class: Table
"""

import logging
import random
from abc import ABC, abstractmethod


class Table(ABC):
    _table_name = None

    @abstractmethod
    def __init__(self, dict_data):
        self._data = dict_data

    @abstractmethod
    def insert(self, cursor):
        pass

    def insert_and_commit(self, conn):
        cursor = conn.cursor()
        self.insert(cursor)
        conn.commit()
        cursor.close()

    @staticmethod
    @abstractmethod
    def create_table(conn):
        pass

    @classmethod
    def fill_with_random_data(cls, conn, min_rows, max_rows):
        cursor = conn.cursor()

        for _ in range(random.randint(min_rows, max_rows)):
            data = cls._randomize_data_row(conn)
            new_row = cls(data)
            new_row.insert(cursor)

        conn.commit()
        cursor.close()

        logging.info("{} table has been filled with random data".format(cls._table_name))

    @staticmethod
    @abstractmethod
    def _randomize_data_row(conn):
        pass
