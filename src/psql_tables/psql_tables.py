"""
File related to creating and deleting tables
"""

import psql_tables
import psql_tables.psql_relationship_tables as relationship_tables

tables = {'customers', 'employees', 'stores', 'stores_employees', 'stores_products', 'transactions'}
tables.update(product_table for product_table in psql_tables.products_tables.tables)


def does_all_tables_exist(conn):
    cursor = conn.cursor()

    cursor.execute("SELECT tablename FROM pg_tables WHERE tablename IN %s", [tuple(tables)])
    tables_info = cursor.fetchall()
    tables_names = set(row[0] for row in tables_info)

    conn.commit()
    cursor.close()

    return tables_names == tables


def create_tables(conn):
    psql_tables.stores.Store.create_table(conn)
    psql_tables.products_tables.create_tables(conn)
    psql_tables.employees.Employee.create_table(conn)
    psql_tables.customers.Customer.create_table(conn)
    relationship_tables.stores_products.StoreProduct.create_table(conn)
    relationship_tables.stores_employees.StoreEmployee.create_table(conn)
    relationship_tables.transactions.Transaction.create_table(conn)

    psql_tables.customers.Customer.create_triggers(conn)
    psql_tables.psql_relationship_tables.stores_products.StoreProduct.create_triggers(conn)
    psql_tables.psql_relationship_tables.transactions.Transaction.create_triggers(conn)


def drop_tables(conn):
    cursor = conn.cursor()

    for table in tables:
        cursor.execute("DROP TABLE IF EXISTS {} CASCADE".format(table))

    conn.commit()
    cursor.close()


def recreate_tables(conn):
    drop_tables(conn)
    create_tables(conn)


def fill_tables_with_random_data(conn):
    psql_tables.customers.Customer.fill_with_random_data(conn, min_rows=50_000, max_rows=100_000)
    psql_tables.employees.Employee.fill_with_random_data(conn, min_rows=3_250, max_rows=4_500)
    psql_tables.stores.Store.fill_with_random_data(conn, min_rows=600, max_rows=750)
    psql_tables.products_tables.fill_tables_with_random_data(conn, min_products=1_500, max_products=3_000)

    # max_rows should be 3x less than employees rows (due to the fact that during the drawing of data, the employee was employed a maximum of 3 times)
    relationship_tables.stores_employees.StoreEmployee.fill_with_random_data(conn, min_rows=4_500, max_rows=8_000)
    relationship_tables.stores_products.StoreProduct.fill_with_random_data(conn, min_rows=125_000, max_rows=175_000)
    relationship_tables.transactions.Transaction.fill_with_random_data(conn, min_rows=200_000, max_rows=250_000)
