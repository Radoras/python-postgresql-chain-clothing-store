"""
The many-to-many relationship table for the stores and customers tables.
It is responsible for storing information about the amount of items purchased in stores by specific customers.
It also updates columns in tables: 'customers' and 'stores_products'
"""

import json
import logging
import random
from datetime import datetime, date

from dateutil.relativedelta import relativedelta

from pg_helper import *
from psql_tables.table import Table


class Transaction(Table):
    _table_name = 'transactions'
    __stores_data = None
    __customers_data = None
    __stores_open_hour_in_seconds = 8 * 60 * 60
    __stores_close_hour_in_seconds = 22 * 60 * 60

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        cols = ['store_id', 'customer_email', 'products', 'purchase_datetime']

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        if full_data['purchase_datetime'] is None:
            full_data['purchase_datetime'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        products = full_data['products']
        products_json = json.dumps(products)
        full_data['products'] = products_json

        if products:
            cursor.execute("INSERT INTO {} (store_id, customer_email, products, purchase_datetime) VALUES (%s, %s, %s, %s)"
                           .format(Transaction._table_name), [full_data[col] for col in cols])
        else:
            cursor.execute("SELECT id FROM transactions ORDER BY id DESC LIMIT 1")
            last_transaction = cursor.fetchall()
            new_transaction_id = 0
            if last_transaction:
                new_transaction_id = last_transaction[0][0] + 1
            logging.warning("No products were added to transaction #{}. The row has not been inserted into the database!"
                            .format(new_transaction_id))

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE {} (
                id SERIAL PRIMARY KEY,
                store_id INT NOT NULL REFERENCES stores(id),
                customer_email email NOT NULL REFERENCES customers(email),
                products JSONB NOT NULL,    -- key: product's barcode; value: number of products
                purchase_datetime TIMESTAMP NOT NULL DEFAULT NOW(),
                price FLOAT NOT NULL,
                profit FLOAT NOT NULL
            )""".format(Transaction._table_name))

    @staticmethod
    def create_triggers(conn):
        cursor = conn.cursor()

        cursor.execute("""
                CREATE OR REPLACE FUNCTION update_all_prices_and_amount()
                    RETURNS TRIGGER
                    LANGUAGE plpgsql AS
                $BODY$
                DECLARE
                    _barcode CHAR(10);
                    _amount  SMALLINT;
                    _products_price  INT;
                    _products_profit INT;
                    _stores_products_total_profit FLOAT;
                    _customers_total_payment FLOAT;
                    _amount_available SMALLINT;
                BEGIN
                    NEW.price  := 0.0;
                    NEW.profit := 0.0;
    
                    FOR _barcode, _amount IN (SELECT * FROM jsonb_each(NEW.products))
                    LOOP
                        _products_price := 0;
                        _products_profit := 0;
    
                        SELECT (selling_price * _amount) AS products_price
                            INTO _products_price
                            FROM products
                            WHERE barcode LIKE _barcode
                            LIMIT 1;
    
                        NEW.price  := NEW.price  + _products_price;
    
                        SELECT (_products_price - (order_price * _amount)) AS products_profit
                            INTO _products_profit
                            FROM products
                            WHERE barcode LIKE _barcode
                            LIMIT 1;
    
                        NEW.profit := NEW.profit + _products_profit;
    
                        SELECT total_profit, amount
                            INTO _stores_products_total_profit, _amount_available
                            FROM stores_products
                            WHERE store_id=NEW.store_id AND product_barcode = _barcode;
    
                        _stores_products_total_profit := _stores_products_total_profit + _products_profit;
                        _amount_available := _amount_available - _amount;
    
                        UPDATE stores_products
                            SET total_profit = _stores_products_total_profit,
                                amount = _amount_available
                            WHERE store_id = NEW.store_id AND product_barcode LIKE _barcode;
    
                        SELECT total_payment
                            INTO _customers_total_payment
                            FROM customers
                            WHERE email = NEW.customer_email;
    
                        _customers_total_payment := _customers_total_payment + _products_price;
    
                        UPDATE customers
                            SET total_payment = _customers_total_payment
                            WHERE email = NEW.customer_email;
                    END LOOP;
    
                    RETURN NEW;
                END;
                $BODY$""")

        cursor.execute("DROP TRIGGER IF EXISTS adjust_all_prices_and_amount ON {}".format(Transaction._table_name))
        cursor.execute("""
                CREATE TRIGGER adjust_all_prices_and_amount
                BEFORE INSERT OR UPDATE ON {}
                FOR EACH ROW
                EXECUTE PROCEDURE update_all_prices_and_amount()
            """.format(Transaction._table_name))

        conn.commit()
        cursor.close()

    @staticmethod
    def _randomize_data_row(conn):
        data = dict()
        cursor = conn.cursor()

        store_py_id = random.randint(0, len(Transaction.__stores_data) - 1)
        customer_py_id = random.randint(0, len(Transaction.__customers_data) - 1)

        data['store_id'] = Transaction.__stores_data[store_py_id]['id']
        data['customer_email'] = Transaction.__customers_data[customer_py_id][0]
        data['products'] = dict()

        various_products_number = random.randint(1, 8)
        cursor.execute("""
            SELECT product_barcode, amount
            FROM stores_products
            WHERE store_id=%s AND product_barcode IN (
                SELECT product_barcode
                FROM stores_products
                WHERE store_id=%s AND amount > 0
                ORDER BY RANDOM()
                LIMIT %s
            )""", [data['store_id'], data['store_id'], various_products_number])
        for _ in range(various_products_number):
            try:
                barcode, amount = cursor.fetchone()
            except Exception:    # no more products in the store
                break

            amount = random.randint(1, min(3, amount))
            data['products'][barcode] = amount

        store_start_date = Transaction.__stores_data[store_py_id]['start_date']
        customer_birth_date = Transaction.__customers_data[customer_py_id][1]
        customer_possible_start_date = customer_birth_date + relativedelta(years=12)
        possible_latest_date = max(store_start_date, customer_possible_start_date)

        delta = date.today() - possible_latest_date
        days_from_today = random.randint(0, delta.days)


        data['purchase_datetime'] = date.today() - relativedelta(days=days_from_today) + \
                                    relativedelta(seconds=random.randint(Transaction.__stores_open_hour_in_seconds,
                                                                         Transaction.__stores_close_hour_in_seconds))

        return data

    @classmethod
    def fill_with_random_data(cls, conn, min_rows, max_rows):
        Transaction.__data_for_randomize(conn)
        cursor = conn.cursor()

        for _ in range(random.randint(min_rows, max_rows)):
            data = cls._randomize_data_row(conn)
            new_row = cls(data)
            new_row.insert(cursor)
            conn.commit()

        conn.commit()
        cursor.close()

        logging.info("{} table has been filled with random data".format(cls._table_name))
        Transaction.__clear_data_for_randomize()

    @staticmethod
    def __data_for_randomize(conn, refind=False):
        if refind or not Transaction.__stores_data:
            cursor = conn.cursor()

            cursor.execute("SELECT id FROM stores")
            Transaction.__stores_data = fetchall_single_column(cursor)

            cursor.execute("SELECT email, birth_date FROM customers")
            Transaction.__customers_data = cursor.fetchall()

            no_employees_stores = list()
            for i, store_data in enumerate(Transaction.__stores_data):
                cursor.execute("""
                    WITH stores_employees_id AS (
                        SELECT store_id, start_date
                        FROM stores_employees
                        WHERE store_id=%s
                    )
                    SELECT start_date
                    FROM stores_employees_id
                    WHERE start_date=(
                        SELECT start_date
                        FROM stores_employees_id
                        ORDER BY start_date
                        LIMIT 1
                    )""", [store_data])
                try:
                    store_start_date = cursor.fetchall()[0][0]
                    Transaction.__stores_data[i] = {'id': store_data, 'start_date': store_start_date}
                except Exception:    # no employee yet
                    no_employees_stores.append(i)

            for i in sorted(no_employees_stores, reverse=True):
                del Transaction.__stores_data[i]

            cursor.close()

    @staticmethod
    def __clear_data_for_randomize():
        del Transaction.__stores_data[:]
        del Transaction.__stores_data
        del Transaction.__customers_data[:]
        del Transaction.__customers_data
