"""
The many-to-many relationship table for the stores and employees tables.
It is responsible for storing information about where the employee works
and about former workplaces of former and current employees in the company
"""

import random
from datetime import date

from dateutil.relativedelta import relativedelta

from pg_helper import *
from psql_tables.table import Table


class StoreEmployee(Table):
    _table_name = 'stores_employees'
    __stores_data = None
    __employees_data = None

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        cols = ['store_id', 'employee_id', 'start_date', 'end_date']

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        if full_data['start_date'] is None:
            full_data['start_date'] = date.today().strftime("%Y-%m-%d")

        cursor.execute("INSERT INTO {} VALUES (%s, %s, %s, %s)".format(StoreEmployee._table_name), [full_data[col] for col in cols])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE {} (
                store_id INT NOT NULL REFERENCES stores(id),
                employee_id INT NOT NULL REFERENCES employees(id),
                start_date DATE NOT NULL DEFAULT CURRENT_DATE CHECK (start_date <= CURRENT_DATE),
                end_date DATE CHECK (end_date <= CURRENT_DATE),
                PRIMARY KEY (store_id, employee_id, start_date), -- start_date because the employee could go back to the same store
                CHECK (start_date <= end_date)
            )""".format(StoreEmployee._table_name))

    @staticmethod
    def _randomize_data_row(conn):
        data = dict()
        cursor = conn.cursor()

        employee_id = None
        employee_birth_date = None
        start_dates = []
        end_dates = []
        while not employee_id:
            employee_py_id = random.randint(0, len(StoreEmployee.__employees_data) - 1)
            employee_id = StoreEmployee.__employees_data[employee_py_id][0]
            employee_birth_date = StoreEmployee.__employees_data[employee_py_id][1]

            cursor.execute("SELECT start_date, end_date FROM stores_employees WHERE employee_id=%s", [employee_id])
            results = cursor.fetchall()
            if results:
                start_dates, end_dates = map(list, zip(*results))
            else:
                start_dates = list()
                end_dates = list()

            recently = date.today() - relativedelta(months=3)

            if len(start_dates) > 3 or (start_dates and start_dates[-1] >= recently):
                employee_id = None
                continue
        data['employee_id'] = employee_id
        data['store_id'] = random.choice(StoreEmployee.__stores_data)

        employee_possible_start_date = employee_birth_date + relativedelta(years=18)
        delta = 0
        if end_dates and end_dates[-1]:
            delta = date.today() - end_dates[-1]
        else:
            delta = date.today() - (start_dates[-1] if start_dates else employee_possible_start_date)
        min_start_date = date.today() - relativedelta(days=delta.days)

        data['start_date'] = min_start_date + relativedelta(days=random.randint(0, delta.days))
        if start_dates:
            delta_end_start = data['start_date'] - start_dates[-1]
            end_date = start_dates[-1] + relativedelta(days=random.randint(0, delta_end_start.days))
            cursor.execute("UPDATE stores_employees SET end_date=%s WHERE employee_id=%s AND end_date=NULL", [end_date, employee_id])

        data['end_date'] = None
        has_end_date = random.choices([True, False], [85, 15])[0]
        if has_end_date and data['start_date'] < date.today():
            delta = date.today() - data['start_date']
            data['end_date'] = data['start_date'] + relativedelta(days=random.randint(1, delta.days))

        cursor.close()

        return data

    @classmethod
    def fill_with_random_data(cls, conn, min_rows, max_rows):
        StoreEmployee.__data_for_randomize(conn)
        super().fill_with_random_data(conn, min_rows, max_rows)
        StoreEmployee.__clear_data_for_randomize()

    @staticmethod
    def __data_for_randomize(conn, refind=False):
        if refind or not StoreEmployee.__stores_data:
            cursor = conn.cursor()

            cursor.execute("SELECT id FROM stores")
            StoreEmployee.__stores_data = fetchall_single_column(cursor)

            cursor.execute("SELECT id, birth_date FROM employees")
            StoreEmployee.__employees_data = cursor.fetchall()

            cursor.close()

    @staticmethod
    def __clear_data_for_randomize():
        del StoreEmployee.__stores_data[:]
        del StoreEmployee.__stores_data
        del StoreEmployee.__employees_data[:]
        del StoreEmployee.__employees_data
