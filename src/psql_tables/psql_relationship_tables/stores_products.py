"""
The many-to-many relationship table for the stores and products tables.
It is responsible for storing information about the amount of products in stores.
Also, if the product is not listed, the store is not selling it at all.
"""

import random

from pg_helper import *
from psql_tables.table import Table


class StoreProduct(Table):
    _table_name = 'stores_products'
    __stores_data = None
    __products_data = None

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        cols = ['store_id', 'product_barcode', 'amount', 'discount']

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        if not full_data['discount']:
            full_data['discount'] = 0.0

        cursor.execute("INSERT INTO {} (store_id, product_barcode, amount, discount) VALUES (%s, %s, %s, %s)"
                       .format(StoreProduct._table_name), [full_data[col] for col in cols])

    def set_discount(self, cursor, discount):
        cursor.execute("UPDATE stores_products SET discount=%s WHERE store_id=%s AND product_barcode=%s",
                       [discount, self._data['store_id'], self._data['product_barcode']])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE {} (
                store_id INT NOT NULL REFERENCES stores(id),
                product_barcode CHAR(10) NOT NULL,    -- not FK because PK is not inherited in PostgreSQL
                amount SMALLINT NOT NULL DEFAULT 0 CHECK (amount >= 0),
                total_profit FLOAT NOT NULL DEFAULT 0,
                discount FLOAT NOT NULL DEFAULT 0 CHECK (discount BETWEEN 0 AND 1),
                PRIMARY KEY (store_id, product_barcode)
            )""".format(StoreProduct._table_name))

    @staticmethod
    def create_triggers(conn):
        cursor = conn.cursor()

        cursor.execute("""
                CREATE OR REPLACE FUNCTION set_total_profit_to_zero()
                    RETURNS TRIGGER
                    LANGUAGE plpgsql AS
                $BODY$
                BEGIN
                    NEW.total_profit := 0.0;
                    RETURN NEW;
                END;
                $BODY$""")

        cursor.execute("""
                CREATE OR REPLACE FUNCTION does_barcode_exists()
                    RETURNS TRIGGER
                    LANGUAGE plpgsql AS
                    $BODY$
                    BEGIN
                        IF NOT EXISTS(SELECT barcode FROM products WHERE barcode LIKE NEW.product_barcode) THEN
                            RAISE EXCEPTION 'The specified barcode does not exist in products table!';
                        END IF;
                        RETURN NEW;
                    END;
                    $BODY$""")

        cursor.execute("DROP TRIGGER IF EXISTS block_insert_total_profit ON {}".format(StoreProduct._table_name))
        cursor.execute("""
                CREATE TRIGGER block_insert_total_profit
                    BEFORE INSERT ON {}
                    FOR EACH ROW
                    EXECUTE PROCEDURE set_total_profit_to_zero()
            """.format(StoreProduct._table_name))

        cursor.execute("DROP TRIGGER IF EXISTS check_if_barcode_exists ON {}".format(StoreProduct._table_name))
        cursor.execute("""
                CREATE TRIGGER check_if_barcode_exists
                    BEFORE INSERT OR UPDATE ON {}
                    FOR EACH ROW
                    EXECUTE PROCEDURE does_barcode_exists()
            """.format(StoreProduct._table_name))

        conn.commit()
        cursor.close()

    @staticmethod
    def _randomize_data_row(conn):
        data = dict()
        cursor = conn.cursor()

        pk_exists = True
        while pk_exists:
            data['store_id'] = random.choice(StoreProduct.__stores_data)
            data['product_barcode'] = random.choice(StoreProduct.__products_data)

            cursor.execute("SELECT EXISTS(SELECT 1 FROM stores_products WHERE store_id=%s AND product_barcode=%s)",
                           [data['store_id'], data['product_barcode']])
            pk_exists = cursor.fetchone()[0]

        data['amount'] = random.randint(8, 75)

        has_discount = random.choices([True, False], [3, 97])[0]
        if has_discount:
            data['discount'] = random.randint(0, 50) / 100

        cursor.close()

        return data

    @classmethod
    def fill_with_random_data(cls, conn, min_rows, max_rows):
        StoreProduct.__data_for_randomize(conn)
        super().fill_with_random_data(conn, min_rows, max_rows)
        StoreProduct.__clear_data_for_randomize()

    @staticmethod
    def __data_for_randomize(conn, refind=False):
        if refind or not StoreProduct.__stores_data:
            cursor = conn.cursor()

            cursor.execute("SELECT id FROM stores")
            StoreProduct.__stores_data = fetchall_single_column(cursor)

            cursor.execute("SELECT barcode FROM products")
            StoreProduct.__products_data = fetchall_single_column(cursor)

            cursor.close()

    @staticmethod
    def __clear_data_for_randomize():
        del StoreProduct.__stores_data[:]
        del StoreProduct.__stores_data
        del StoreProduct.__products_data[:]
        del StoreProduct.__products_data
