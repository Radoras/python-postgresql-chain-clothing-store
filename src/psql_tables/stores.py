"""
Stores table
"""

from data import *
from pg_helper import *
from psql_tables.table import Table


class Store(Table):
    _table_name = 'stores'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        cols = ['country', 'city', 'postal_code', 'street_name', 'street_number', 'apartment_number']

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        cursor.execute("INSERT INTO {} (address) VALUES (ROW(%s, %s, %s, %s, %s, %s))".format(Store._table_name),
                       [full_data[col] for col in cols])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE {} (
                id SERIAL PRIMARY KEY,
                address ADDRESS NOT NULL
            )""".format(Store._table_name))

    @staticmethod
    def _randomize_data_row(conn):
        address = generate_random_address()
        return address
