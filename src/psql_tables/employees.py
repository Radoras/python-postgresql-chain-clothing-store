"""
Employees table
"""

from dateutil.relativedelta import relativedelta

from data import *
from pg_helper import *
from psql_tables.table import Table


class Employee(Table):
    _table_name = 'employees'

    def __init__(self, dict_data):
        super().__init__(dict_data)

    def insert(self, cursor):
        employee_data_cols = ['name', 'surname', 'account_number', 'birth_date', 'phone_number']
        address_cols = ['country', 'city', 'postal_code', 'street_name', 'street_number', 'apartment_number']
        cols = employee_data_cols + address_cols

        dict_empty_data = dict.fromkeys(cols)
        full_data = {**dict_empty_data, **self._data}

        if full_data['country']:
            cursor.execute("""INSERT INTO {} (name, surname, account_number, birth_date, phone_number, address)
                              VALUES (%s, %s, %s, %s, %s, ROW(%s, %s, %s, %s, %s, %s))""".format(Employee._table_name),
                           [full_data[col] for col in cols])
        else:
            cursor.execute("""INSERT INTO {} (name, surname, account_number, birth_date, phone_number)
                              VALUES (%s, %s, %s, %s, %s)""".format(Employee._table_name),
                           [full_data[col] for col in employee_data_cols])

    @staticmethod
    def create_table(conn):
        generic_sql(conn, """
            CREATE TABLE {} (
                id serial PRIMARY KEY,
                name VARCHAR(40) NOT NULL CHECK (name ~ '^[A-Za-z]+$'),
                surname VARCHAR(40) NOT NULL CHECK (surname ~ '^[A-Za-z]+$'),
                account_number VARCHAR(12) NOT NULL CHECK (account_number ~ '^[0-9]*$' AND LENGTH(account_number) >= 10),
                birth_date DATE NOT NULL CHECK (birth_date <= CURRENT_DATE - INTERVAL '18 years'),
                phone_number VARCHAR(15) CHECK (phone_number ~ '^[0-9]*$' AND LENGTH(phone_number) >= 8),
                address ADDRESS
            )""".format(Employee._table_name))

    @staticmethod
    def _randomize_data_row(conn):
        highest_birth_date = datetime.date.today() - relativedelta(years=18)
        lowest_birth_date = datetime.date.today() - relativedelta(years=50)
        birth_date = random_date(lowest_birth_date, highest_birth_date)

        gender = random.choice(['m', 'f'])
        name = random.choice(male_names if gender == 'm' else female_names)
        surname = random.choice(surnames)

        account_number = generate_account_number()
        phone_number = generate_phone_number()

        gave_address = random.choices([True, False], [80, 20])[0]
        address = generate_random_address() if gave_address else None

        if address:
            return {**{'name': name, 'surname': surname, 'account_number': account_number, 'birth_date': birth_date,
                       'phone_number': phone_number}, **address}
        else:
            return {'name': name, 'surname': surname, 'account_number': account_number, 'birth_date': birth_date,
                    'phone_number': phone_number}
