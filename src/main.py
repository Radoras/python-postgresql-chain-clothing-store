"""
Before running the script, log in to PostgreSQL as the postgres user and run the following commands:

CREATE DATABASE chain_clothing_store;
CREATE USER username WITH PASSWORD 'password';
GRANT ALL ON DATABASE chain_clothing_store TO username;
"""

import argparse
import logging

from connector import Connector
from psql_tables import psql_tables
from psql_types import psql_types

logging.basicConfig(level=logging.INFO)


def recreate_db(conn):
    psql_types.recreate_types(conn)
    psql_tables.recreate_tables(conn)
    psql_tables.fill_tables_with_random_data(conn)


def is_all_exists(conn):
    return psql_types.does_all_types_exist(conn) and psql_tables.does_all_tables_exist(conn)


def main():
    db = 'chain_clothing_store'

    parser = argparse.ArgumentParser()
    parser.add_argument('username', help='Username with which you can log in to the PostgreSQL "{}" database and work freely on it (GRANT ALL PRIVILEGES).'.format(db), type=str)
    parser.add_argument('password', help="Password to the entered username.", type=str)
    args = parser.parse_args()

    conn = Connector.connect(db, args.username, args.password)

    if_recreate_db = True
    if is_all_exists(conn):
        if_recreate_db = input("Do you want to recreate database? [y/n] ") == 'y'
    if if_recreate_db:
        recreate_db(conn)

    conn.close()


if __name__ == '__main__':
    main()
