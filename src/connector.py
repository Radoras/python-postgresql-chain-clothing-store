"""
File of the class responsible for creating a connection to the database and operating on this connection
"""

import psycopg2
import logging


class Connector:
    @staticmethod
    def connect(db, username, password):
        conn = None

        try:
            conn = psycopg2.connect(database=db, host='localhost', user=username, password=password)
        except psycopg2.DatabaseError:
            logging.warning("Failed to connect to the database '{}'!".format(db))
            exit(1)

        return conn
